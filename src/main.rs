#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

use rocket::response::NamedFile;
use rocket::request::{Form, FromForm};
use rocket::http::RawStr;
use pandoc::{OutputKind, PandocOutput, PandocError, PandocOption};
use std::path::{Path, PathBuf};
use std::fs;

#[derive(FromForm)]
struct Document {
    file: String,
    text: String,
}

#[get("/view/<file..>")]
fn files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("output/").join(file)).ok()
}

#[get("/raw/<file..>")]
fn raw(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("documents/").join(file)).ok()
}

#[get("/")]
fn index() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/index.html")).ok()
}

#[get("/edit")]
fn edit() -> Option<NamedFile> {
    NamedFile::open(Path::new("static/edit.html")).ok()
}

#[post("/save", data = "<data>")]
fn save(data: Form<Document>) -> String {
    println!("{}:\n {}", &data.file, &data.text);
    match fs::write(Path::new("documents/").join(&data.file), &data.text) {
        Ok(_) => "Saved!".into(),
        Err(error) => format!("Error: {}", error.to_string()),
    }
}

#[get("/build/<ext>/<name>")]
fn build(ext: &RawStr, name: &RawStr) -> String {
    match compile(name.as_str(), ext.as_str()) {
        Ok(_) => format!("Compiled {} successfully!", name.as_str()),
        Err(error) => format!("Error: {}", error.to_string()),
    }
}

fn compile(document: &str, ext: &str) -> Result<PandocOutput, PandocError> {
    let mut pandoc = pandoc::new();
    pandoc.add_input(&format!("documents/{}.md", document));

    if ext == "html" {
        pandoc.add_option(PandocOption::MathJax(None));
        pandoc.add_option(PandocOption::Standalone);
    }

    pandoc.set_output(OutputKind::File(format!("output/{}.{}", document, ext).into()));
    pandoc.execute()
}

fn main() {
    rocket::ignite().mount("/", routes![index, edit, save, raw, build, files]).launch();
}
